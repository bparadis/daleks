
# -*- coding: UTF-8 -*-

################################################
# Classe Controleur
################################################
#     consulterModele
#        
    def validerDeplacementDr
        if self.x > j.x:
            self.x =self.x -1
        if self.x < j.x:
            self.x =self.x +1
        si tas de feraille
            déplacement refusé
        si mur haut
            déplacement refusé
        si mur gauche
            déplacement refusé
        si mur droite
            déplacement refusé
        si mur bas
            déplacement refusé

    validerDeplacementDaleks
        si mur haut
            déplacement refusé
        si mur gauche
            déplacement refusé
        si mur droite
            déplacement refusé
        si mur bas
            déplacement refusé

    actualiserPartieEnCours
    
     def validerTouche(self,touche):
        if touche == "1":      # Bas + Gauche
            return True
        elif touche == "2":    # Bas
            return True
        elif touche == "3":    # Bas + Droite
            return True
        elif touche == "4":    # Gauche
            return True
        elif touche == "5":    # Bouge pas
            return True
        elif touche == "6":    # Droite
            return True
        elif touche == "7":    # Haut-Gauche
            return True
        elif touche == "8":    # Haut
            return True
        elif touche == "9":    # Haut + Droite
            return True
        elif touche == "t":    # Teleporteur
            return True
        elif touche == "z":    # Zappeur
            return True

################################################
# Classe Vue
################################################
#    afficherMenuPrincipal
#        1. nouvelle partie
#        2. meilleurs scores
#        3. quitter
#
#    afficherPartieEnCours
#        afficher aire de jeu
#        afficherMenuDr
#        
#    afficherMenuDr
#        a. avancer
#        b. zapper (slmt disponible si Dalek autour du Dr)
#
#        c. téléporter (aléatoire pr l'instant)
#
#        si avancer
#            afficher:
#        "utilisez le clavier numérique pr vous déplacer"
#               ^      7  8  9
#            <- + ->      4  5  6
#               v      1  2  3
#
#    afficherMenuFinDePartie
#        si Dr est mort
#            afficher "vous avez perdu"
#            afficherMenuPrincipal
#        si tous les Daleks sont morts
#            afficher "vous avez gagné"
#

################################################
# Classe Modele
################################################

    nbDalekParNiveau = 5

#    choisirMenu
#        si Dr n'existe pas
#            afficherMenuPrincipal
#        si Dr est vivant
#            afficherPartieEnCours
#        si Dr est mort
#            afficherMenuFinDePartie
#        si tous les Daleks sont morts
#            afficherMenuFinDePartie

    def choisirMenu(self):
        repValide = False
        while not repValide:                    # Préparation du jeu
            self.a.clear()                      
            self.m.ouvrirHighscore()            
            self.a.afficherMenuPrincipal()      
            reponse = self.a.getChoix()         # Attendre l'entrée du joueur :
            if reponse == "1":                  # 1 pour jouer au jeu
                repValide = True
                self.m.lancerJeu()
            elif reponse == "2":                # 2 pour accéder aux options du jeu
                self.option()
            elif reponse == "3":                # 3 pour afficher les résultats/classements
                self.a.afficherHighscore(self.m.highscore)
            elif reponse == "4":                # 4 pour quitter le jeu
                self.m.quitter()
                
        def creerNouvellePartie(self,niveau, points):
        self.daleks = []
        self.niveau = niveau
        self.nbDaleks = self.nbDalekParNiveau * niveau
        self.points = points
        for i in range(self.nbDaleks):
            self.daleks.append(Dalek(self))
                
#
#    creerNouvellePartie
#

################################################
# Classe PartieEnCours
################################################
#    calculerPoints
#        points + 5 points par Dalek
#
#    collisionDaleks
#        collision avec un tas de feraille
#            le dalek meurt
#            5 points de plus pr le joueur
#        collision avec un autre Dalek
#            morts = []
#            for i in self.Daleks:
#                for j in self.Daleks:
#                    if i != j:
#                        if(i.x==j.x)and(i.y==j.y):
#                            morts.append(i)
#            for i in morts:
#                self.Dalek.remove
#
#    drEstMort
#        ???
#
#    nouveauNiveau
#        dessiner aire de jeu
#        placer dr
#        placer Daleks
#        placer tas de feraille
#        score = 0
#        niveauNo + 1
#
#    finDePartie
#        ???

################################################
# Classe Dr
################################################
#    zapper
#        si aucun Dalek autour du Dr
#            refuser
#        sinon
#            détruire les Daleks autour du Dr
#
#    teleporter (aléatoire pr l'instant)
#        position du Dr = random...
#
#    deplacerDr
#        sans valider, change la position du Dr
#        7 = 
#        8 = 
#        9 = 
#        4 = 
#        5 = 
#        6 = 
#        1 = 
#        2 = 
#        3 = 

################################################
# Classe Daleks
################################################
#    deplacerDaleks

################################################
# Classe TasFeraille
################################################



