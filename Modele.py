# -*- coding: UTF-8 -*-

################################################
# Classe Modele
################################################

nbDalekParNiveau = 5

#    choisirMenu
#        si Dr n'existe pas
#            afficherMenuPrincipal
#        si Dr est vivant
#            afficherPartieEnCours
#        si Dr est mort
#            afficherMenuFinDePartie
#        si tous les Daleks sont morts
#            afficherMenuFinDePartie

def choisirMenu(self):
    repValide = False
    while not repValide:                    # Préparation du jeu
        self.a.clear()                      
        self.m.ouvrirHighscore()            
        self.a.afficherMenuPrincipal()      
        reponse = self.a.getChoix()         # Attendre l'entrée du joueur :
        if reponse == "1":                  # 1 pour jouer au jeu
            repValide = True
            self.m.lancerJeu()
        elif reponse == "2":                # 2 pour accéder aux options du jeu
            self.option()
        elif reponse == "3":                # 3 pour afficher les résultats/classements
            self.a.afficherHighscore(self.m.highscore)
        elif reponse == "4":                # 4 pour quitter le jeu
            self.m.quitter()
            
    def creerNouvellePartie(self,niveau, points):
        self.daleks = []
        self.niveau = niveau
        self.nbDaleks = self.nbDalekParNiveau * niveau
        self.points = points
        for i in range(self.nbDaleks):
            self.daleks.append(Dalek(self))
