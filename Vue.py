
# -*- coding: UTF-8 -*-

#/////////////////////////////////////////////////////////////////////////////////////
#// PROPRIÉTAIRE: Alexis Duval
#// AUTEUR: Alexis Duval
#// DATE: 27 août 2018
#// FICHIER: Vue.py
#// DESCRIPTION: Classe Vue, classe qui affiche les infos à l'écran pour le joueur
#//                et qui reçoit les entrées du joueur.
#/////////////////////////////////////////////////////////////////////////////////////

class Vue():
    def __init__(self, parent):
        self.parent = parent

    def afficherMenuPrincipal(self):
        pass

    def affichePartieEnCours(self):
        afficherAireDeJeu()
        afficherMenuDr()
    
    def afficherAireDeJeu(self, aire):
        aire = [1,2,3] # À MODIFIER
        print ("Voici votre aire de jeu")
        for i in aire:
            print (i)
    
    def afficherMenuDr(self):
        print ("Effectuez votre choix:")
        print ("a: avancer")
        print ("b: téléporter (aléatoirement)")
        if true: # s'il y a au moins un Dalek autour du Dr:
            print ("c: zapper")
        if avancer:
            print("Utilisez le clavier numérique pr vous déplacer")
        
#         a. avancer
#         b. téléporter (aléatoire pr l'instant)
#         c. zapper (slmt disponible si Dalek autour du Dr)
#         si avancer:
#             afficher:
#                    "utilisez le clavier numérique pr vous déplacer"


    def afficherMenuFinDePartie(self):
        pass
#         si Dr est mort
#             afficher "vous avez perdu"
#             afficherMenuPrincipal
#         si tous les Daleks sont morts
#             afficher "vous avez gagné"

















